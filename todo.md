

- [ ] Découverte des bases d'Obsidian :
	- [ ] les liens
	- [ ] les tags
	- [ ] les checkboxs
- [ ] Découverte de PlugIns :
	- [ ] Templater
	- [ ] En bonus : Dataview & DataviewJS (mode avancé)
- [ ] Le graph !
	- [ ] Ne pas visualiser le répertoire 'system'
	- [ ] Colorer les notes en fonction des tags