#Tutoriel [[git]]


## Connexion et récupération d'un repository privé *(avec identification)*
```mermaid 
flowchart TD
subgraph Déroulement
Gen[1a Génération des clefs] --> conf[1b Configuration de leur utilisation] --> enr[2a Enregistrement sur le service Git] --> cnx[2b Connexion au service] --> util[3 Récupération du projet]
end
```

Pre-requis : Avoir git installé sur son systeme

### 1. Génerer & indiquer l'utilisation d'une paire de clef SSH
![[ssh, genereration de clefs#Génération d'une paire de clef SSH]]
![[ssh, genereration de clefs#Indiquer à qui va servir votre paire de clefs générée]]

### 2. Renseigner la clef publique dans votre service GIT
Exemple avec Gitlab : 
![[Pasted image 20211128002939.png]] 
*(Pour vous faciter les captures d'écran, n'hesitez pas à utiliser des outils comme [[ShareX]])*

1. Cliquer sur votre icone d'utilisateur
2. Aller dans les préférences
3. Ouvrir le menu 'SSH Keys'
4. Coller le contenu de votre clef publique (ex: `C:\users\truc\.ssh\gitlab_cesi.pub`)

#### Vérification
Depuis le terminal, tapper  : 
```bash
ssh -T {nom/adresse_de_l'hote_pour_vous}
```

Exemples : 
```bash
ssh -T gitlab.com
```
```bash
ssh -T gitlab.com.cesi
```

### 3. Cloner le repo privé

```bash 
git clone git@gitlab.com:scepia1/levelup/labs.git
```

Dans le cas où vous avez indiqué un Host différent du Hostname :
```bash
git clone git@gitlab.com.cesi:scepia1/levelup/labs.git
```



