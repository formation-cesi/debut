 #outil/logiciel
# Résumé
Secure Shell est à la fois un programme informatique et un protocole de communication sécurisé. Le protocole de connexion impose un échange de clés de chiffrement en début de connexion. Par la suite, tous les segments TCP sont authentifiés et chiffrés.

# Infos

# Tutoriels :
![[ssh, genereration de clefs]]

