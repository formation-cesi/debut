#tutoriel [[ssh]]


### Génération d'une paire de clef SSH
1. Creer le répertoire `C:\users\{votre_user}\.ssh` s'il n'existe pas
2. Utiliser ssh-keygen pour générer la paire de clefs ssh (une publique & privé)
	- La clef publique sera la clef que vous aller diffuser aux tiers (Gitlab, Github, server linux, ..)
	- La clef privé sera gardé secrète
		- *Attention : sous Linux, si les droits sur ce fichier ne sont pas restreint, il vous sera impossible d'utiliser votre clef*
```bash
 ssh-keygen -t rsa
 ```
Il vous sera demandé la destination :
Indiquer `C:\users\{votre_user}\.ssh\{a_quoi_va_servir_cette_clef}`
Exemple `C:\users\truc\.ssh\gitlab_cesi`

### Indiquer à qui va servir votre paire de clefs générée 
1. Créer & éditer le fichier `C:\users\{votre_user}\.ssh\.config`
2. Ajouter dans le fichier les informations permettant de relier un tier (hote) à un utilisateur et une clef privé
```
#{a_qui/quoi_va_servir_cette_clef}
Host {nom/adresse_de_l'hote_pour_vous}
  HostName {nom/adresse_du_tier_reel}
  User {nom_de_l'utilisateur}
  IdentityFile ~/.ssh/{nom_du_fichier_de_la_clef_privée}
```

Exemple pour une connexion à [[gitlab]] : 
```
#Gitlab.com [CESI]
Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/gitlab_cesi
```

Exemple pour une connexion à [[gitlab]] si vous avez deja un compte [[gitlab]] : 
```
#Gitlab.com [CESI]
Host gitlab.com.cesi
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/gitlab_cesi
```